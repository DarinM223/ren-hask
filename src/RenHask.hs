{-# LANGUAGE OverloadedStrings #-}
module RenHask (module R) where

import RenHask.Add as R
import RenHask.Params as R
import RenHask.Screen as R
import RenHask.Toplevel as R
import RenHask.TransformProps as R

import Data.Text
import Data.Text.IO as T
import RenHask.ToText

end :: Monad m => m ()
end = return ()

test :: Text
test = run $ child $ screen "screen" $ params ["hello"] $ do
  zorder 1
  modal True

  child $ screen "screen2" end
  child $ screen "screen3" $ params ["a" := "hello", "b" := false, "c" := 2] $
    zorder 10
  child $ screen "screen4" $ params ["hello", "d" := 3, "world"] $ variant "b"
  child $ screen "screen5" $ params ["e" := (1 + 2), "f" := (-3)] $ zorder 10
  child $ screen "screen6" $ props [zorder 10] $ params ["a" := 2] $ tag "foo"
  child $ screen "screen7" $ props [zorder 1, modal True] $ variant "a"

  child $ add "image.png" $ props [xalign 1.0, yalign 0.0] end

printTest :: IO ()
printTest = T.putStrLn test
