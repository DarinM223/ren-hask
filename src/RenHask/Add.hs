{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
module RenHask.Add where

import Control.Monad.State
import Data.Text (Text)
import GHC.Generics
import RenHask.ToText
import RenHask.TransformProps

data AddData = AddData
  { _image          :: Text
  , _transformProps :: TransformProps
  } deriving (Show, Generic)

instance ToText AddData where
  toText i (AddData t props) =
    indent i <> "add " <> toText i t <> " " <> toText i props <> "\n"

newtype AddT m a = Add { unAdd :: StateT AddData m a }
  deriving (Functor, Applicative, Monad, MonadState AddData)

add :: Monad m => Text -> AddT m a -> m AddData
add t m = execStateT (unAdd m) (AddData t [])
