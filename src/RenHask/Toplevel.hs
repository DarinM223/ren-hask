{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE UndecidableInstances #-}
module RenHask.Toplevel
  ( TopLevel
  , HasChildren
  , run
  , child
  ) where

import Control.Monad.State
import Data.Generics.Product.Fields
import Data.Text (Text)
import GHC.Generics
import Lens.Micro
import RenHask.ToText

newtype TopLevelData = TopLevelData { _children :: Children }
  deriving (Show, Generic)
instance ToText TopLevelData where
  toText i s = childrenToText i (_children s)

newtype TopLevel a = TopLevel { unTopLevel :: State TopLevelData a }
  deriving (Functor, Applicative, Monad, MonadState TopLevelData)

run :: TopLevel a -> Text
run m = toText 0 $ execState (unTopLevel m) (TopLevelData [])

class HasField' "_children" a Children => HasChildren a
instance HasField' "_children" a Children => HasChildren a

child :: (Show a, ToText a, MonadState s m, HasChildren s) => m a -> m ()
child m = m >>= \child' -> modify' $ field' @"_children" %~ (Textable child':)

data Opaque
instance {-# OVERLAPPING #-} HasField' "_children" Opaque Children where
  field' = undefined
instance {-# OVERLAPPING #-} HasChildren Opaque
