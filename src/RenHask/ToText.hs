{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE OverloadedStrings #-}
module RenHask.ToText
  ( ToText (..)
  , Textable (Textable)
  , Children
  , childrenToText
  , indent
  , true
  , false
  ) where

import Data.List (intersperse)
import Data.String (IsString (fromString))
import Data.Text (Text)
import TextShow (showt)
import qualified Data.Text as T

class ToText s where
  toText :: Int  -- ^ Indent level
         -> s    -- ^ Object to convert into text
         -> Text -- ^ Text result

instance ToText Int where toText _ = showt
instance ToText Float where toText _ = showt
instance ToText Text where toText _ = showt
instance ToText Bool where toText _ = T.toLower . showt

data Textable = forall s. (Show s, ToText s) => Textable s | TextableInt Int

instance IsString Textable where
  fromString = Textable . T.pack
instance Num Textable where
  (+) (TextableInt a) (TextableInt b) = TextableInt $ a + b
  (+) _ _                             = error "+ not defined for Textable"

  (*) (TextableInt a) (TextableInt b) = TextableInt $ a * b
  (*) _ _                             = error "* not defined for Textable"

  abs (TextableInt s) = TextableInt $ abs s
  abs _               = error "abs not defined for Textable"

  signum (TextableInt s) = TextableInt $ signum s
  signum _               = error "signum not defined for Textable"

  negate (TextableInt s) = TextableInt $ negate s
  negate _               = error "negate not defined for Textable"

  fromInteger i = TextableInt (fromIntegral i :: Int)

instance Show Textable where
  show (Textable s)    = show s
  show (TextableInt s) = show s
instance ToText Textable where
  toText i (Textable s)    = toText i s
  toText i (TextableInt s) = toText i s

type Children = [Textable]

childrenToText :: Int -> Children -> Text
childrenToText i = mconcat . intersperse "\n" . fmap (toText i) . reverse

indent :: Int -> Text
indent i = T.replicate (i * 4) " "

true :: Textable
true = Textable True

false :: Textable
false = Textable False
