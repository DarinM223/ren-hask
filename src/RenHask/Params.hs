{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE UndecidableInstances #-}
module RenHask.Params
  ( Param (..)
  , HasParams
  , params
  , props
  ) where

import Control.Monad.State
import Data.Generics.Product.Fields
import Data.String (IsString (fromString))
import Data.Text
import Lens.Micro
import RenHask.ToText

data Param = Param Text | Text := Textable
  deriving Show

instance IsString Param where
  fromString = Param . pack

instance ToText Param where
  toText _ (Param name)    = name
  toText i (name := value) = name <> " = " <> toText i value

instance ToText [Param] where
  toText i params = "(" <> paramsStr <> ")"
   where paramsStr = intercalate ", " $ fmap (toText i) params

class HasField' "_params" a [Param] => HasParams a
instance HasField' "_params" a [Param] => HasParams a

params :: (MonadState s m, HasParams s) => [Param] -> m a -> m a
params ps m = modify' (field' @"_params" .~ ps) >> m

props :: Monad m => [m a] -> m a -> m a
props ps m = sequence_ ps >> m

data Opaque
instance {-# OVERLAPPING #-} HasField' "_params" Opaque [Param] where
  field' = undefined
instance {-# OVERLAPPING #-} HasParams Opaque
