{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE UndecidableInstances #-}
module RenHask.TransformProps
  ( TransformProp
  , TransformProps
  , pos
  , xpos
  , ypos
  , anchor
  , xanchor
  , yanchor
  , align
  , xalign
  , yalign
  , xoffset
  , yoffset
  , xcenter
  , ycenter
  , rotate
  , rotatePad
  , transformAnchor
  , zoom
  , xzoom
  , yzoom
  , alpha
  , additive
  , around
  , alignAround
  , angle
  , radius
  , crop
  , corner1
  , corner2
  , subpixel
  , delay
  ) where

import Control.Monad.State
import Data.Generics.Product.Fields
import Data.List (intersperse)
import Lens.Micro
import RenHask.ToText

data TransformProp = Pos (Int, Int)
                   | XPos Int
                   | YPos Int
                   | Anchor (Int, Int)
                   | XAnchor Int
                   | YAnchor Int
                   | Align (Float, Float)
                   | XAlign Float
                   | YAlign Float
                   | XOffset Float
                   | YOffset Float
                   | XCenter Float
                   | YCenter Float
                   | Rotate (Maybe Float)
                   | RotatePad Bool
                   | TransformAnchor Bool
                   | Zoom Float
                   | XZoom  Float
                   | YZoom Float
                   | Alpha Float
                   | Additive Float
                   | Around (Int, Int)
                   | AlignAround (Float, Float)
                   | Angle Float
                   | Radius Int
                   | Crop (Maybe (Int, Int, Int, Int))
                   | Corner1 (Maybe (Int, Int))
                   | Corner2 (Maybe (Int, Int))
                   | Size (Maybe (Int, Int))
                   | Subpixel Bool
                   | Delay Float
                   deriving (Show, Eq)

instance ToText TransformProp where
  toText i = \case
    Pos (a, b) -> "pos (" <> toText i a <> ", " <> toText i b <> ")"
    XPos x -> "xpos " <> toText i x
    YPos y -> "ypos " <> toText i y
    Anchor (a, b) -> "anchor (" <> toText i a <> ", " <> toText i b <> ")"
    XAnchor x -> "xanchor" <> toText i x
    YAnchor y -> "yanchor" <> toText i y
    Align (a, b) -> "align (" <> toText i a <> ", " <> toText i b <> ")"
    XAlign x -> "xalign " <> toText i x
    YAlign y -> "yalign " <> toText i y
    XOffset x -> "xoffset " <> toText i x
    YOffset y -> "yoffset " <> toText i y
    XCenter x -> "xcenter " <> toText i x
    YCenter y -> "ycenter " <> toText i y
    Rotate m -> "rotate " <> maybe "None" (toText i) m
    RotatePad b -> "rotate_pad " <> toText i b
    TransformAnchor b -> "transform_anchor " <> toText i b
    Zoom f -> "zoom " <> toText i f
    XZoom x -> "xzoom " <> toText i x
    YZoom y -> "yzoom " <> toText i y
    Alpha f -> "alpha " <> toText i f
    Additive f -> "additive " <> toText i f
    Around (a, b) -> "around (" <> toText i a <> ", " <> toText i b <> ")"
    AlignAround (a, b) ->
      "alignaround (" <> toText i a <> ", " <> toText i b <> ")"
    Angle f -> "angle " <> toText i f
    Radius r -> "radius " <> toText i r
    Crop m -> "crop " <> maybe "None" (parens . convertList . fourTuple) m
    Corner1 m -> "corner1 " <> maybe "None" (parens . convertList . twoTuple) m
    Corner2 m -> "corner2 " <> maybe "None" (parens . convertList . twoTuple) m
    Size m -> "size " <> maybe "None" (parens . convertList . twoTuple) m
    Subpixel b -> "subpixel " <> toText i b
    Delay f -> "delay " <> toText i f
   where
    parens t = "(" <> t <> ")"
    convertList l = mconcat $ intersperse ", " $ fmap (toText i) l
    twoTuple (a, b) = [a, b]
    fourTuple (a, b, c, d) = [a, b, c, d]

type TransformProps = [TransformProp]

instance ToText [TransformProp] where
  toText i = mconcat . intersperse " " . fmap (toText i) . reverse

class HasField' "_transformProps" d [TransformProp] => HasTransformProps d
instance HasField' "_transformProps" d [TransformProp] => HasTransformProps d

handle :: (MonadState s m, HasTransformProps s) => TransformProp -> m ()
handle v = modify $ field' @"_transformProps" %~ (v:)

pos :: (MonadState s m, HasTransformProps s) => (Int, Int) -> m ()
pos = handle . Pos

xpos :: (MonadState s m, HasTransformProps s) => Int -> m ()
xpos = handle . XPos

ypos :: (MonadState s m, HasTransformProps s) => Int -> m ()
ypos = handle . YPos

anchor :: (MonadState s m, HasTransformProps s) => (Int, Int) -> m ()
anchor = handle . Anchor

xanchor :: (MonadState s m, HasTransformProps s) => Int -> m ()
xanchor = handle . XAnchor

yanchor :: (MonadState s m, HasTransformProps s) => Int -> m ()
yanchor = handle . YAnchor

align :: (MonadState s m, HasTransformProps s) => (Float, Float) -> m ()
align = handle . Align

xalign :: (MonadState s m, HasTransformProps s) => Float -> m ()
xalign = handle . XAlign

yalign :: (MonadState s m, HasTransformProps s) => Float -> m ()
yalign = handle . YAlign

xoffset :: (MonadState s m, HasTransformProps s) => Float -> m ()
xoffset = handle . XOffset

yoffset :: (MonadState s m, HasTransformProps s) => Float -> m ()
yoffset = handle . YOffset

xcenter :: (MonadState s m, HasTransformProps s) => Float -> m ()
xcenter = handle . XCenter

ycenter :: (MonadState s m, HasTransformProps s) => Float -> m ()
ycenter = handle . YCenter

rotate :: (MonadState s m, HasTransformProps s) => Maybe Float -> m ()
rotate = handle . Rotate

rotatePad :: (MonadState s m, HasTransformProps s) => Bool -> m ()
rotatePad = handle . RotatePad

transformAnchor :: (MonadState s m, HasTransformProps s) => Bool -> m ()
transformAnchor = handle . TransformAnchor

zoom :: (MonadState s m, HasTransformProps s) => Float -> m ()
zoom = handle . Zoom

xzoom :: (MonadState s m, HasTransformProps s) => Float -> m ()
xzoom = handle . XZoom

yzoom :: (MonadState s m, HasTransformProps s) => Float -> m ()
yzoom = handle . YZoom

alpha :: (MonadState s m, HasTransformProps s) => Float -> m ()
alpha = handle . Alpha

additive :: (MonadState s m, HasTransformProps s) => Float -> m ()
additive = handle . Additive

around :: (MonadState s m, HasTransformProps s) => (Int, Int) -> m ()
around = handle . Around

alignAround :: (MonadState s m, HasTransformProps s) => (Float, Float) -> m ()
alignAround = handle . AlignAround

angle :: (MonadState s m, HasTransformProps s) => Float -> m ()
angle = handle . Angle

radius :: (MonadState s m, HasTransformProps s) => Int -> m ()
radius = handle . Radius

crop :: (MonadState s m, HasTransformProps s)
     => Maybe (Int, Int, Int, Int) -> m ()
crop = handle . Crop

corner1 :: (MonadState s m, HasTransformProps s) => Maybe (Int, Int) -> m ()
corner1 = handle . Corner1

corner2 :: (MonadState s m, HasTransformProps s) => Maybe (Int, Int) -> m ()
corner2 = handle . Corner2

subpixel :: (MonadState s m, HasTransformProps s) => Bool -> m ()
subpixel = handle . Subpixel

delay :: (MonadState s m, HasTransformProps s) => Float -> m ()
delay = handle . Delay

data Opaque
instance {-# OVERLAPPING #-}
  HasField' "_transformProps" Opaque [TransformProp] where
  field' = undefined
instance {-# OVERLAPPING #-} HasTransformProps Opaque
