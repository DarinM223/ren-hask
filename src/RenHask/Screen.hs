{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
module RenHask.Screen
  ( ScreenData (..)
  , screen
  , modal
  , tag
  , zorder
  , variant
  ) where

import Control.Monad.State
import Data.Default
import Data.Generics.Product.Fields
import Data.Monoid (mconcat)
import Data.Text (Text)
import GHC.Generics
import Lens.Micro
import RenHask.Params
import RenHask.ToText

data ScreenProps = ScreenProps
  { _modal   :: Bool
  , _tag     :: Text
  , _zorder  :: Int
  , _variant :: Text
  } deriving (Show, Generic)

instance ToText ScreenProps where
  toText i s = mconcat
    [ indent i <> "modal " <> toText i (_modal s) <> "\n"
    , indent i <> "tag " <> toText i (_tag s) <> "\n"
    , indent i <> "zorder " <> toText i (_zorder s) <> "\n"
    , indent i <> "variant " <> toText i (_variant s)
    ]

instance Default ScreenProps where
  def = ScreenProps { _modal   = False
                    , _tag     = ""
                    , _zorder  = 0
                    , _variant = "" }

data ScreenData = ScreenData
  { _name     :: Text
  , _params   :: [Param]
  , _props    :: ScreenProps
  , _children :: Children
  } deriving (Show, Generic)

instance ToText ScreenData where
  toText i s = mconcat
    [ indent i <> "screen " <> screenName <> ":\n"
    , toText (i + 1) (_props s) <> "\n"
    , newline <> childrenToText (i + 1) (_children s)
    ]
   where
    screenName | null (_params s) = _name s
               | otherwise        = _name s <> toText i (_params s)
    newline = if null (_children s) then "" else "\n"

newtype ScreenT m a = Screen { unScreen :: StateT ScreenData m a }
  deriving (Functor, Applicative, Monad, MonadState ScreenData)

screen :: Monad m => Text -> ScreenT m a -> m ScreenData
screen n m = execStateT (unScreen m) (ScreenData n [] def [])

modal :: Monad m => Bool -> ScreenT m ()
modal b = modify' $ field @"_props" . field @"_modal" .~ b

tag :: Monad m => Text -> ScreenT m ()
tag t = modify' $ field @"_props" . field @"_tag" .~ t

zorder :: Monad m => Int -> ScreenT m ()
zorder z = modify' $ field @"_props" . field @"_zorder" .~ z

variant :: Monad m => Text -> ScreenT m ()
variant v = modify' $ field @"_props" . field @"_variant" .~ v
